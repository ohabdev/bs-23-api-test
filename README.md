# [Brain Station memes sharing API Test using Node.js]
Brain Station API test project

## Requirements

- [NodeJS](https://nodejs.org/en/download/) >= v13.14.0,
- NPM >= 6.14.4
- MongoDB
- Radis Server

## Installation

- Install all softwares above
- Run `npm install`
- Run `npm run dev` to start `Test BS API` in development environment

### User login endpoint : http://localhost:9000/v1/auth/login

- Method: POST
- Request Body Payload: 
- {
-	"email": "ohabdev@gmail.com",
-	"password": "ohabdev"
- }

### User Register endpoint : http://localhost:9000/v1/auth/register

- Method: POST
- Request Body Payload: 
- {
-	"email": "ohabdev@gmail.com",
-	"password":"ohabdev",
-	"phoneNumber": "01640191679",
-	"name": "Ohab"
- }

### Image Upload endpoint : http://localhost:9000/v1/media/photos

- Method: POST
- Key: file
- Content Type: form-data

### Image short by top Likes & Views endpoint : http://localhost:9000/v1/media/search
- Method: GET

# Image edit & delete DONE.
### Image edit : http://localhost:9000/v1/media/:id
- Method: PUT

### Image delete : http://localhost:9000/v1/media/photos/:id
- Method: DELETE

# Useless Image will be automatically removed after certain period of time DONE
# View increment and Like value updated DONE.