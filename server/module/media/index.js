const fs = require('fs');
const path = require('path');
const multer = require('multer');
const config = require('./config');

const uploadPhoto = multer({
  storage: multer.diskStorage({
    destination(req, file, cb) {
      cb(null, config.photoDir);
    },
    filename(req, file, cb) {
      const ext = Helper.String.getExt(file.originalname);
      const nameWithoutExt = Helper.String.createAlias(Helper.String.getFileName(file.originalname, true));
      let fileName = `${nameWithoutExt}${ext}`;
      if (fs.existsSync(path.resolve(config.photoDir, fileName))) {
        fileName = `${nameWithoutExt}-${Helper.String.randomString(5)}${ext}`;
      }
      cb(null, fileName);
    },
    fileSize: (process.env.MAX_PHOTO_SIZE || 10) * 1024 * 1024 // 10MB limit
  })
});

// the queue
require('./queue');

const photoController = require('./controllers/photo.controller');
const mediaController = require('./controllers/media.controller');

exports.model = {
  Media: require('./models/media'),
};

exports.services = {
  Media: require('./services/Media')
};

exports.router = (router) => {


  /**
   * @apiDefine photoRequest
   * @apiParam {Object}   file  file data
   * @apiParam {String}   [name] file name. Otherwiwse it is
   * @apiParam {String}   [description] photo description
   * @apiParam {String}   [description]
   * @apiParam {String[]} [categoryIds] categories which this photo belongs to
   */

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {post} /v1/media/photos  Upload a photo
   * @apiDescription Upload a photo. Use multipart/form-data to upload file and add additional fields
   * @apiUse authRequest
   * @apiUse photoRequest
   * @apiPermission user
   */
  router.post(
    '/v1/media/photos',
    Middleware.isAuthenticated,
    uploadPhoto.single('file'),
    photoController.base64Upload,
    photoController.upload,
    Middleware.Response.success('photo')
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {get} /v1/media/search?:page&:take&:name&:type&:sort&:sortType Get list media
   * @apiDescription Get list media
   * @apiParam {Number}   [page="1"]
   * @apiParam {Number}   [take="10"]
   * @apiParam {String}   [name]
   * @apiParam {String}   [type] `video`, `photo`...
   * @apiParam {Sring}   [sort="createdAt"]
   * @apiParam {Sring}   [sortType="desc"]
   * @apiPermission user
   */
  router.get(
    '/v1/media/search',
    Middleware.isAuthenticated,
    mediaController.search,
    Middleware.Response.success('search')
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {get} /v1/media/:id Get media
   * @apiDescription Get a media detail
   * @apiParam {String}   id        media id
   * @apiPermission all
   */
  router.get(
    '/v1/media/:id',
    mediaController.findOne,
    Middleware.Response.success('media')
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {get} /v1/media/:id Get photo detail
   * @apiDescription Get a video detail
   * @apiParam {String}   id        photo id
   * @apiPermission all
   */
  router.get(
    '/v1/media/photos/:id',
    mediaController.findOne,
    Middleware.Response.success('media')
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {delete} /v1/media/photos/:id Remove a photo
   * @apiDescription Remove a photo
   * @apiUse authRequest
   * @apiParam {String}   id        photo id
   * @apiPermission user
   */
  router.delete(
    '/v1/media/photos/:id',
    Middleware.isAuthenticated,
    mediaController.findOne,
    mediaController.validatePermission,
    mediaController.remove,
    Middleware.Response.success('remove')
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {delete} /v1/media/:id Remove a media
   * @apiDescription Remove a media
   * @apiUse authRequest
   * @apiParam {String}   id        media id
   * @apiPermission user
   */
  router.delete(
    '/v1/media/:id',
    Middleware.isAuthenticated,
    mediaController.findOne,
    mediaController.validatePermission,
    mediaController.remove,
    Middleware.Response.success(PopulateResponse.deleteSuccess())
  );

  /**
   * @apiGroup Media
   * @apiVersion 1.0.0
   * @api {put} /v1/media/:id Update a media
   * @apiDescription Update a media
   * @apiUse authRequest
   * @apiParam {String}   id        media id
   * @apiParam {String}   name        media name
   * @apiParam {String}   description        media description
   * @apiParam {String[]}   categoryIds        media category
   * @apiPermission user
   */
  router.put(
    '/v1/media/:id',
    Middleware.isAuthenticated,
    mediaController.findOne,
    mediaController.validatePermission,
    mediaController.update,
    Middleware.Response.success('update')
  );
};
