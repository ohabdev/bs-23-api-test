const Image = require('../components/image');
const Q = require('../queue');

exports.createPhoto = async (options) => {
  try {
    const file = options.file;
    const value = options.value;
    const user = options.user;
    const photo = new DB.Media({
      type: 'photo',
      systemType: value.systemType,
      name: value.name || file.filename,
      mimeType: file.mimetype,
      description: value.description,
      likes: 0,
      views: 0,
      uploaderId: user._id,
      // ownerId: user.role === 'admin' && value.ownerId ? value.ownerId : user._id,
      categoryIds: value.categoryIds,
      originalPath: file.path,
      filePath: file.path,
      convertStatus: 'done',
      // uploaded: process.env.USE_S3 !== 'true'
    });
    await photo.save();

    // if (!photo.uploaded && process.env.USE_S3 === 'true') {
    //   await Q.uploadS3(photo);
    // }

    return photo;
  } catch (e) {
    throw e;
  }
};
