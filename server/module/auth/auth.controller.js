const Joi = require('joi');
const nconf = require('nconf');
const url = require('url');

exports.register = async (req, res, next) => {
  const schema = Joi.object().keys({
    type: Joi.string().allow(['user']).default('user'),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
    phoneNumber: Joi.string().allow(['', null]).optional(),
    name: Joi.string().allow(['', null]).optional()
  });

  const validate = Joi.validate(req.body, schema);
  if (validate.error) {
    return next(PopulateResponse.validationError(validate.error));
  }

  try {
    const count = await DB.User.count({
      email: validate.value.email.toLowerCase()
    });
    if (count) {
      return next(PopulateResponse.error({
        message: 'This email has already taken'
      }, 'ERR_EMAIL_ALREADY_TAKEN'));
    }

    const user = new DB.User(validate.value);
    user.emailVerifiedToken = Helper.String.randomString(48);
    await user.save();

    // now send email verificaiton to user
    // await Service.Mailer.send('verify-email.html', user.email, {
    //   subject: 'Verify email address',
    //   emailVerifyLink: url.resolve(nconf.get('baseUrl'), `v1/auth/verifyEmail/${user.emailVerifiedToken}`)
    // });

    res.locals.register = PopulateResponse.success({
      message: 'Your account has been created, please verify your email address and get access.'
    }, 'USE_CREATED');
    return next();
  } catch (e) {
    return next(e);
  }
};
