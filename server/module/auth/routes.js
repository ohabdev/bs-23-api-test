const authController = require('./auth.controller');
const localController = require('./local');

module.exports = (router) => {
  /**
   * @apiGroup Auth
   * @apiVersion 1.0.0
   * @api {post} /v1/auth/login  Local login
   * @apiDescription Login with email and password
   * @apiParam {String}   email      email address
   * @apiParam {String}   password   password
   * @apiSuccessExample {json} Success-Response:
   *  {
   *     "code": 200,
   *     "message": "OK",
   *     "data": {
   *         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9....",
   *         "expiredAt": "2018-09-14T06:39:18.140Z"
   *     },
   *     "error": false
   *  }
   * @apiPermission all
   */
  router.post(
    '/v1/auth/login',
    localController.login,
    Middleware.Response.success('login')
  );

  /**
   * @apiGroup Auth
   * @apiVersion 1.0.0
   * @api {post} /v1/auth/register  Register new account
   * @apiDescription Login with email and password
   * @apiParam {String}   email      email address
   * @apiParam {String}   password   password
   * @apiParam {String}   [phoneNumber]  phone number
   * @apiParam {String}   [name] user name
   * @apiSuccessExample {json} Success-Response:
   *  {
   *      "code": 200,
   *       "message": "OK",
   *      "data": {
   *           "code": 200,
   *           "httpCode": 200,
   *           "error": false,
   *           "message": "USE_CREATED",
   *           "data": {
   *               "message": "Your account has been created, please verify your email address and get access."
   *           }
   *       },
   *       "error": false
   *  }
   * @apiPermission all
   */
  router.post(
    '/v1/auth/register',
    authController.register,
    Middleware.Response.success('register')
  );

};
